<?php

spl_autoload_register('includeClass');

/**
 * Load routes
 */
$routesFile = __DIR__.'/src/routes.php';
if (file_exists($routesFile)) {
    require $routesFile;
}

function includeClass($class)
{
    $basePath = __DIR__.'/src/';

    $class = str_replace('\\', '/', $class);
    $file = $basePath.$class.'.php';

    if (file_exists($file)) {
        require $file;
    }
}
