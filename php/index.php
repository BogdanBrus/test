<?php

require_once ('bootstrap.php');

use Core\Kernel;
$kernel = new Kernel;

echo $kernel->handle();
