<?php

namespace Controllers;

use Core\Request;
use Core\Response;
use Services\SearchPhoneService;

class SearchPhoneController
{
    /**
     * Search Phone Form
     * @return false|string
     */
    public function getSearchPhoneForm()
    {
        return Response::view('search_phone_form');
    }

    /**
     * Find Phone
     * POST
     */
    public function findPhone()
    {
        /** @var Request $request */
        $request = Request::getInstance();
        $areacode =  $request->getParameters('areacode');
        $phone =  $request->getParameters('phone');

        //TODO add validation
        
        $searchPhoneService = new SearchPhoneService();
        $contacts = $searchPhoneService->findContacts($areacode, $phone);

        //TODO add validation if problem on xml server
        return Response::view('contacts', $contacts);
    }
}
