<?php

namespace Services;

class SearchPhoneService
{
    private $baseUrl = 'https://www.infopay.com/phptest_phone_xml.php';
    private $baseParams = [
        'username' => 'accucomtest',
        'password' => 'test104',
    ];

    /**
     * @param int $areacode
     * @param int $phone
     * @return array|null
     */
    public function findContacts(int $areacode, int $phone): ?array
    {
       $urlParams = $this->preparedUrlParams($areacode, $phone);

       $content = $this->getContent($urlParams);

       if (null === $content) {
            return null;
       }
       
       return $this->getDataFromContent($content);
    }

    /**
     * @param array $content
     * @return array
     */
    private function getDataFromContent(array $content): array
    {
        $records = $content['record'] ?? [];

        $contacts = [];
        foreach ($records as $record) {
            $firstName = $record['firstname'] ?? '';
            $lastName = $record['lastname'] ?? '';
            $fullName = $firstName.' '.$lastName;
            $state = $record['state'];
            $contacts[] = [
                'fullName' => $fullName,
                'state' => $state,
            ];
        }

        return $contacts;
    }



    /**
     * @param int $areacode
     * @param int $phone
     * @return string
     */
    private function preparedUrlParams(int $areacode, int $phone)
    {
        $phoneParams = [
            'areacode' => $areacode,
            'phone' => $phone,
        ];
        $params = array_merge($this->baseParams, $phoneParams);

        return http_build_query($params);
    }

    /**
     * @param $urlParams
     * @return array|null
     */
    private function getContent($urlParams): ?array
    {
        $urlWithParams = $this->baseUrl.'?'.$urlParams;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlWithParams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        try {
            $xml = simplexml_load_string($output);
            $json = json_encode($xml);
            $arr = json_decode($json,true);
        } catch (\Exception $e) {
            return null;
        }

        return $arr;
    }
}
