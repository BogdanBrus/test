<?php

use Core\Router;

$router = Router::getInstance();

$router->get('/', 'SearchPhoneController', 'getSearchPhoneForm');
$router->post('/', 'SearchPhoneController', 'findPhone');
