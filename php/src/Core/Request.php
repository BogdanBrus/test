<?php

namespace Core;

class Request
{
    private $method;
    private $uri;
    private $router;

    private $controller;
    private $action;
    private $parameters;

    private static $request;

    /**
     * Request constructor.
     */
    private function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->router = Router::getInstance();
    }

    /**
     * Single
     *
     * @return Request
     */
    public static function getInstance(): ?self
    {
        if (!self::$request) {
            /** @var Request $request */
            $request = new self();

            $routeData = $request->router->findRoute($request->method, $request->uri);

            if (null === $routeData) {
                return null;
            }

            $request->setController($routeData['controller']);
            $request->setAction($routeData['action']);
            $request->setParameters();

            self::$request = $request;
        }

        return self::$request;
    }

    /**
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $param
     * @return mixed|null
     */
    public function getParameters($param = '')
    {
        return $this->parameters[$param] ?? null;
    }

    /**
     * @param $controller
     */
    private function setController($controller): void
    {
        $controllerClass = 'Controllers\\'.$controller;

        $this->controller = new $controllerClass;
    }

    /**
     * @param $action
     */
    private function setAction($action): void
    {
        $this->action = $action;
    }

    /**
     * set parameters from get or post
     */
    private function setParameters(): void
    {
        switch ($this->method) {
            case 'GET':
                $parameters = $_GET;
                break;
            case 'POST':
                $parameters = $_POST;
                break;
            default:
                $parameters = [];
                break;
        }

        $this->parameters = $parameters;
    }
}
