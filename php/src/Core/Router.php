<?php

namespace Core;

class Router implements RouterInterface
{
    public const GET = 'GET';
    public const POST = 'POST';
    public const DELETE = 'DELETE';
    public const PUT = 'PUT';

    private static $router;
    private $routesMap;

    /**
     * Router constructor.
     */
    private function __construct() {}

    /**
     * @return Router
     */
    public static function getInstance()
    {
        if (!self::$router) {
            self::$router = new self();
        }

        return self::$router;
    }

    /**
     * @param string $method
     * @param string $uri
     * @return array|null
     */
    public function findRoute(string $method, string $uri): ?array
    {
        $routesMap = $this->routesMap;
        if (!isset($routesMap[$method])) {
            return null;
        }

        return $routesMap[$method][$uri] ?? null;
    }

    /**
     * @param string $uri
     * @param string $controller
     * @param string $action
     * @return RouterInterface
     */
    public function get(string $uri, string $controller, string $action): RouterInterface
    {
        return $this->setRoutesMap(self::GET, $controller, $action, $uri);
    }

    /**
     * @param string $uri
     * @param string $controller
     * @param string $action
     * @return RouterInterface
     */
    public function post(string $uri, string $controller, string $action): RouterInterface
    {
        return $this->setRoutesMap(self::POST, $controller, $action, $uri);
    }

    /**
     * @param string $uri
     * @param string $controller
     * @param string $action
     * @return RouterInterface
     */
    public function put(string $uri, string $controller, string $action): RouterInterface
    {
        return $this->setRoutesMap(self::PUT, $controller, $action, $uri);
    }

    /**
     * @param string $uri
     * @param string $controller
     * @param string $action
     * @return RouterInterface
     */
    public function delete(string $uri, string $controller, string $action): RouterInterface
    {
        return $this->setRoutesMap(self::DELETE, $controller, $action, $uri);
    }

    /**
     * @param $method
     * @param $controller
     * @param $action
     * @param $uri
     * @return RouterInterface
     */
    private function setRoutesMap($method, $controller, $action, $uri): RouterInterface
    {
        if (!isset($this->routesMap[$method][$uri])) {
            $this->routesMap[$method][$uri] = [];
        }

        return $this->setController($this->routesMap[$method][$uri], $controller)
            ->setAction($this->routesMap[$method][$uri], $action);
    }

    /**
     * @param array $routeItem
     * @param string $controller
     * @return Router
     */
    private function setController(array &$routeItem, string $controller): self
    {
        $routeItem['controller'] = $controller;

        return $this;
    }

    /**
     * @param array $routeItem
     * @param string $action
     * @return Router
     */
    private function setAction(array &$routeItem, string $action): self
    {
        $routeItem['action'] = $action;

        return $this;
    }
}
