<?php

namespace Core;

class Kernel
{
    /** @var Request|null if not valid route */
    private $request;

    /**
     * Kernel constructor.
     */
    public function __construct()
    {
        $this->request = Request::getInstance();
    }

    /**
     * Handle request and return response
     * @return string
     */
    public function handle(): string
    {
        if (null === $this->request) {
            return Response::pageNotFound();
        }

        $controller = $this->request->getController();
        $action = $this->request->getAction();

        return $controller->{$action}();
    }
}
