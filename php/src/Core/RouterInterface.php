<?php

namespace Core;

interface RouterInterface
{
    /**
     * @param string $uri
     * @param string $controller
     * @param string $action
     * @return RouterInterface
     */
    public function get(string $uri, string $controller, string $action): self;

    /**
     * @param string $uri
     * @param string $controller
     * @param string $action
     * @return RouterInterface
     */
    public function post(string $uri, string $controller, string $action): self;

    /**
     * @param string $uri
     * @param string $controller
     * @param string $action
     * @return RouterInterface
     */
    public function put(string $uri, string $controller, string $action): self;

    /**
     * @param string $uri
     * @param string $controller
     * @param string $action
     * @return RouterInterface
     */
    public function delete(string $uri, string $controller, string $action): self;
}
