<?php

namespace Core;

class Response
{
    /**
     * @param string $view
     * @return false|string
     */
    public static function view(string $view, array $parameters = [])
    {
        $path = __DIR__.'/../Views/'.$view.'.php';

        self::setSuccessHeader();

        ob_start();
        include_once($path);
        $var= ob_get_clean();
        ob_end_clean();

        return $var;
    }

    /**
     * @return string
     */
    public static function pageNotFound()
    {
        self::setNotFoundHeader();

        return 'Page not found!';
    }

    private static function setSuccessHeader()
    {
        header('HTTP/1.1 200 OK');
    }

    private static function setNotFoundHeader()
    {
        header('HTTP/1.1 404');
    }
}
